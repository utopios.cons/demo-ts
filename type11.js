// Union types :
function sum(arg, arg2) {
    var resu;
    if (typeof arg === "number" && typeof arg2 === "number") {
        resu = arg + arg2;
    }
    else if (typeof arg === "string" && typeof arg2 === "string") {
        resu = arg + arg2;
    }
    else {
        resu = arg.toString() + arg2.toString();
    }
    return resu;
}
console.log(sum(20, "bonjour"));
console.log(sum(20, 20));
console.log(sum("lolo", "bonjour"));
console.log(sum("lili", 400));
