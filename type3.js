// **** enum ****
var role;
(function (role) {
    role[role["ADMIN"] = 0] = "ADMIN";
    role[role["EMPLOYEE"] = 1] = "EMPLOYEE";
    role[role["MANAGER"] = 2] = "MANAGER";
    role[role["DIRECTEUR"] = 3] = "DIRECTEUR";
})(role || (role = {}));
var user = {
    nom: "tom",
    age: 12,
    role: role[1]
};
var user2 = {
    nom: "tom",
    age: 12,
    role: role.DIRECTEUR
};
console.log(user);
console.log(user2);
if (user2.role === role.DIRECTEUR) {
    console.log("\n\nBienvenue " + user2.nom + ",\n\nVous avez le statut " + role[3] + ".\n\n");
}
var CardinalDirections;
(function (CardinalDirections) {
    CardinalDirections["North"] = "North";
    CardinalDirections["East"] = "East";
    CardinalDirections["South"] = "South";
    CardinalDirections["West"] = "West";
})(CardinalDirections || (CardinalDirections = {}));
;
console.log(CardinalDirections.North);
var color;
(function (color) {
    color[color["Red"] = 1] = "Red";
    color[color["Blue"] = 2] = "Blue";
    color[color["Green"] = 7] = "Green";
})(color || (color = {}));
console.log(color.Blue);
console.log(color[1]);
console.log(color.Green);
