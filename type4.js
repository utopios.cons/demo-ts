// ****** Type any ******* 
// any marche avec les autres types
var anyData;
anyData = "toto";
console.log(typeof anyData);
anyData = 200;
console.log(typeof anyData);
anyData = true;
console.log(typeof anyData);
anyData = [];
console.log(typeof anyData);
// Tableau any.
var anyArray = ["hello", true, { nom: "Tom", age: 23 }, ["world", 45]];
// Exemple objet vers any.
var person = { nom: "Pierre", age: 34 };
console.log(person);
person = "Je suis une personne";
