// **** Tableau *****
var ville = ["Roubaix", "Croix", "Lens", "Lille", "Bethune", 200];
ville.push("Paris");
console.log(ville);
//let prenom: number = ["Pierre","Paul","Marc","Tom","Alain"];
var prenom = ["Pierre", "Paul", "Marc", "Tom", "Alain"];
prenom[1] = "Sam";
//prenom[2] = 200;
console.log(prenom);
var colors = ["rouge", "bleue", "jaune", "vert", "gris", true];
// **** Tuple *****
var demo;
demo = ["tom", 23];
console.log(demo);
//demo = ["tom",23,"employee"];
demo.push("employee");
console.log(demo);
// Autre exemple
var ourTuple;
// initialize correctly
ourTuple = [5, false, 'Coding God was here'];
// We have no type safety in our tuple for indexes 3+
ourTuple.push('Something new and wrong');
console.log(ourTuple);
// Tuple readonly : 
// define our readonly tuple
var ourReadonlyTuple = [5, true, 'The Real Coding God'];
// throws error as it is readonly.
//ourReadonlyTuple.push('Coding God took a day off');
// Tuple nommés : 
var coord = [55.2, 41.3];
//console.log(coord[0]);
console.log(coord);
// Destructuring tuple
var donnee = [55.2, 41.3];
var x = donnee[0],
    y = donnee[1];
// **** Object *****
// Object
var car = {
    color: "red",
    date: 2022,
    speed: 200
};
// Type Objet 
var voiture;
//voiture.color = "grey";
/* let voiture:object {
    color: string;
    date: number;
    speed: number;
} */
//console.log(voiture.color);
/* const bus: { type: string, mileage: number } = {

    type:"peugoet",
  };
  bus.mileage = 2000; */
var bus = {
    type: "Toyota"
};
bus.mileage = 2000;