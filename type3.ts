

// **** enum ****

enum role {
    ADMIN, // 0
    EMPLOYEE, // 1
    MANAGER, // 2
    DIRECTEUR, // 3
}

const user = {
    nom: "tom",
    age: 12,
    role: role[1]
}

const user2 = {
    nom: "tom",
    age: 12,
    role: role.DIRECTEUR
}


console.log(user);
console.log(user2);


if (user2.role === role.DIRECTEUR) {

    console.log(`

Bienvenue ${user2.nom},

Vous avez le statut ${role[3]}.

`)


}


enum CardinalDirections {
    North = 'North',
    East = "East",
    South = "South",
    West = "West"
};

console.log(CardinalDirections.North);

enum color {
    Red = 1,
    Blue,
    Green = 7
}



console.log(color.Blue);
console.log(color[1]);
console.log(color.Green);
