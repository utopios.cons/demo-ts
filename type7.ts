// Type function : 


function gift(num: number) {

    return num + 12;

}
/* 
let age;
age = gift;
console.log("age 1", age(10))

age = "bonjour";

console.log("age 2", age(22)); */ // age is not a function

// Donnez un type à age pour eviter 

/* let age : Function;
age = gift;
console.log("age 1", age(10))

age = "bonjour";

console.log("age 2", age(22)); */

// on peut être plus precis pour lui preciser que notre function doit retourner quelque chose

/* function displauClg(arg){

    console.log(arg)
    return arg; // rajouter un retour pour que la ligne 40 marche.
}

let age : (num : number) => number;
age = gift;
console.log("age 1", age(10))

age = displauClg;

console.log("age 2", age(22)); */