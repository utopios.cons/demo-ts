// Type unknown


// Voilà pourquoi le unknown

/* let inputData: any;

inputData = true;

let inputAge: number;

inputAge = inputData; */


// l'intérêt unknown

/* let inputData: unknown;

inputData = true;

let inputAge: number;

inputAge = inputData; */


// mais même si c'est un number ça marche pas :


 let inputData: unknown;

inputData = 200;

let inputAge: number;

inputAge = inputData; 


// il faut rajouter une vérrification : 


/* let inputData: unknown;

inputData = 200;

let inputAge: number;

if (typeof inputData === 'number') {

    inputAge = inputData;

    console.log(typeof inputAge);

} */


