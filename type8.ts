// Paramètre facultatifs et par défaut : 


/* function buildName(firstName: string, lastName: string) {

    return firstName + " " + lastName;

} */

/* let resultat = buildName("Pierre", "Louis");

console.log(resultat); */

// 1 ---- il manque un argument : -----

/* function buildName(firstName: string, lastName?: string) {

    return firstName + " " + lastName;

}

let resultat = buildName("Pierre"); // manque un argument

console.log(resultat); */

// ---- 2 argument par défaut : -----


/* function buildName(firstName: string, lastName = "Max") {

    return firstName + " " + lastName;

}

let resultat = buildName("Pierre"); // manque un argument

console.log(resultat); */

// ---- 3 argument en trop : -----


/* function buildName(firstName: string, lastName = "Max") {

    return firstName + " " + lastName;

}

let resultat = buildName("Pierre","Marco","Albert"); // Un argument en trop. Erreur mais marche quand même affiche juste pas le nombre.

console.log(resultat); */

// ---- 4 premier argument par defaut ----

 function buildName(firstName = "Paul", lastName?: string) {

    return firstName + " " + lastName;

}

//let resultat = buildName("Marco"); // Marco undefined

let resultat = buildName(undefined,"Marco"); // Marco undefined


console.log(resultat); 