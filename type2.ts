

// **** Tableau *****

let ville = ["Roubaix", "Croix", "Lens", "Lille", "Bethune", 200];

ville.push("Paris");

console.log(ville);

//let prenom: number = ["Pierre","Paul","Marc","Tom","Alain"];

let prenom: Array<string> = ["Pierre", "Paul", "Marc", "Tom", "Alain"];

prenom[1] = "Sam";

//prenom[2] = 200;

console.log(prenom);

//let colors: (string | boolean)[] = ["rouge", "bleue", "jaune", "vert", "gris", true];



// **** Tuple *****

let demo: [string, number];

demo = ["tom", 23];

console.log(demo);

//demo = ["tom",23,"employee"];

demo.push("employee");

console.log(demo);

// Autre exemple

let ourTuple: [number, boolean, string];

// initialize correctly
ourTuple = [5, false, 'Coding God was here'];

// We have no type safety in our tuple for indexes 3+
ourTuple.push('Something new and wrong');

console.log(ourTuple);

// Tuple readonly : 

// define our readonly tuple
const ourReadonlyTuple: readonly [number, boolean, string] = [5, true, 'The Real Coding God'];
// throws error as it is readonly.
//ourReadonlyTuple.push('Coding God took a day off');


// Tuple nommés : 

const coord: [x: number, y: number] = [55.2, 41.3];

//console.log(coord[0]);
console.log(coord);

// Destructuring tuple


const donnee: [number, number] = [55.2, 41.3];
const [x, y] = donnee;


// **** Object *****

// Object
let car = {
    color: "red",
    date: 2022,
    speed: 200
}

// Type Objet 

let voiture: {
    color: string;
    date: number;
    speed: number;
}

//voiture.color = "grey";

/* let voiture:object {
    color: string;
    date: number;
    speed: number;
} */

//console.log(voiture.color);


/* const bus: { type: string, mileage: number } = { 

    type:"peugoet",
  };
  bus.mileage = 2000; */


  const bus: { type: string, mileage?: number } = { // no error
    type: "Toyota"
  };
  bus.mileage = 2000;


// Destructuring

  const testResults: number[] = [3.89, 2.99, 1.38];
  const [result1, result2, result3] = testResults;

  console.log(result1, result2, result3);


  // Destructuring Objet

  const scientist: { firstNom: string, experience: number } = { firstNom: 'Robert', experience: 9000 };
  const { firstNom, experience } = scientist;

  console.log(firstNom, experience);