// type Rest

// sans Rest : 

/* const divColors = document.getElementById("colors");

let colors = function (arg: string, arg1: string, arg2: string, arg3: string, arg4: string) {

    const h1 = document.createElement("h1");

    h1.innerHTML = `Titre : ${arg}`;
    divColors.appendChild(h1);

    const ul = document.createElement("ul");


    for (let i in arguments) {

        let li = document.createElement("li");
        li.innerHTML = arguments[i];

        ul.appendChild(li)
    }

    divColors.appendChild(ul);

}

colors("Titre1", "Rouge","noir","bleue","jaune");
colors("Titre2", "Red","Black","Blue","Yellow"); */




// Avec Rest : 


/* const divColors = document.getElementById("colors");


let colors = function (arg: string, ...restColor:string[]) {

    const h1 = document.createElement("h1");

    h1.innerHTML = `Titre : ${arg}`;
    divColors.appendChild(h1);

    const ul = document.createElement("ul");


    for (let i in arguments) {

        let li = document.createElement("li");
        li.innerHTML = arguments[i];

        ul.appendChild(li)
    }

    divColors.appendChild(ul);

}

colors("Titre1", "Rouge","noir","bleue","jaune");
colors("Titre2", "Red","Black","Blue","Yellow"); */

// avec Rest et sans le titre dans la liste : http



const divColors = document.getElementById("colors");


let colors = function (arg: string, ...restColor:string[]) {

    const h1 = document.createElement("h1");

    h1.innerHTML = `Titre : ${arg}`;
    divColors.appendChild(h1);

    const ul = document.createElement("ul");


    for (let i in restColor) {

        let li = document.createElement("li");
        li.innerHTML = restColor[i];

        ul.appendChild(li)
    }

    divColors.appendChild(ul);

}

colors("Titre1", "Rouge","noir","bleue","jaune");
colors("Titre2", "Red","Black","Blue","Yellow");