// type par inference : 


let firstName;

// if firstName => let firstName = "papa";

firstName = true;
firstName = 'red';
firstName = 10;


// type par assignation :


let isBoolean: boolean;

// isBoolean = "dodo";

isBoolean = true;



// Exemple avec une fonction :


const sous = (val1, val2) => {

    return val1 - val2;

}

/* const sous2 = (val1, val2) => {

    if (typeof val1 === "number" && typeof val2 === "number") {

        return val1 - val2;

    } else {

        throw new Error("Invalid");
    }

} */

/* const sous = (val1: number, val2: number) =>{

    return val1 - val2;
    
    } */


let resultat = sous(12, 23);

console.log(resultat);

let resultat1 = sous('paul', "pierre");

console.log(resultat1);


