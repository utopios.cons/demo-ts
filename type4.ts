// ****** Type any ******* 

// any marche avec les autres types

let anyData: any;
anyData = "toto";
console.log(typeof anyData);
anyData = 200;
console.log(typeof anyData);
anyData = true;
console.log(typeof anyData);
anyData = [];
console.log(typeof anyData);

// Tableau any.

let anyArray: any[] = ["hello", true, { nom: "Tom", age: 23 }, ["world", 45]];


// Exemple objet vers any.

let person: any = { nom: "Pierre", age: 34 };

console.log(person);

person = "Je suis une personne"